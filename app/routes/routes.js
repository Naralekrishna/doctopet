const multer = require('multer');

module.exports = function (app) {
    const users = require('../controllers/userController.js');
    // const pet = require('../controller/pet.controller.js');
    // const vet = require('../controller/vet.controller.js');

    const path = require('path');
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
        cb(null, './public/images')
        },
        filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now()+path.extname(file.originalname))
        }
    })
    var upload = multer({ storage: storage })

    // App Routes
    // BaseRoute
    app.get('/', (req, res) => {
        return res.status(200).json({ 
            status: 'success',
            message: 'Api is ready.' 
        });
    })
    // Auth Route
    app.post('/api/v1/login', users.login);
    app.post('/api/v1/forgetPassword',users.forgetPassword);
    app.put('/api/v1/resetPassword/:id',users.resetPassword)
    app.post('/api/v1/register',users.register)

//     app.post('/api/AddPet',upload.single('myFile'),pet.AddPet)
//     app.post('/api/EditPet',upload.single('myFile'),pet.EditPet)
//     app.post('/api/GetUsersPet',pet.GetUsersPet)
//     app.post('/api/DeletePet',pet.DeletePet)

//     app.post('/api/AddProducts',upload.single('myFile'),vet.AddProducts)
//     app.post('/api/EditProducts',upload.single('myFile'),vet.EditProducts)
//     app.post('/api/GetProducts',vet.GetProducts)
//     app.post('/api/GetSlots',vet.GetSlots)
//     app.post('/api/BookAppointment',vet.BookAppointment)
}