const nodeMailer = require('nodemailer');
const generator = require('generate-password');
// const randomize = require('randomatic');

const db = require('../configuration/dbConfig');
const config = require('../configuration/config');
const env = require('../configuration/env');

const User = db.user;

// User register
exports.register = async (req, res) => {
    // Validate request
    (!req.body.first_name) ? res.status(401).json({ status: "failed", message: "First Name should not be empty!" }) : true;
    (!req.body.last_name) ? res.status(401).json({ status: "failed", message: "Last Name should not be empty!" }) : true;
    (!req.body.email) ? res.status(401).json({ status: "failed", message: "Email should not be empty!" }) : true;
    (!req.body.password) ? res.status(401).json({ status: "failed", message: "Password should not be empty!" }) : true;
    if(checkEmail(req.body.email)) 
    try {
        const user = await User.create(req.body);
        return res.status(201).json({ status: "success", data: { user } });
    } catch (error) {
        return res.status(401).json({ status: "failed", error });
    }
}

// User login
exports.login = (req, res) => {
    // Validate request
    (!req.body.email) ? res.status(401).json({ status: "failed", message: "Email should not be empty!" }) : true;
    (!req.body.password) ? res.status(401).json({ status: "failed", message: "Password should not be empty!" }) : true;

    // Find user by email
    User.findOne({ where: { email: req.body.email } }).then( user => {
        if (!user) {
            return res.status(401).json({
                status: 'failed',
                message: 'Email is not found!'
            });
        } else {
            if (req.body.password === user.password) {
                const jwt = require('jsonwebtoken');
                const JWTToken1 = jwt.sign({ UserId: user.UserId, id: user.id }, config.secret, {
                });
                res.status(200).json({
                    status: 'success',
                    message: 'Welcome To The Application',
                    token: JWTToken1,
                });
            } else {
                return res.status(401).json({
                    success: 'failed',
                    message: 'Incorrect password!',
                });
            }
        }
    });
}

// Forget password
exports.forgetPassword = async (req, res) => {
    // Validate request
    (!req.body.email) ? res.status(401).json({ status: "failed", message: "Email should not be empty!" }) : true;

    // Find User by Id
    try {
        const user = await User.findOne({ where: { email: req.body.email } });
        if(!user) return res.status(401).json({ status: "failed", message: "Email not found!" });
        // Generat Password
        let password = generator.generate({
            length: 5,
            numbers: true
        });
        // Configuring Nodemailer
        let transporter = nodeMailer.createTransport({
            host: "smtp.mailtrap.io",
            port: 2525,
            auth: {
            user: "278d957b8999b4",
            pass: "0519453cf7b71a"
            }
        });
        // Setting up mail
        let mailOptions = {
			from: `<krishna.narale@rajyugsolutions.com>`,
			to: user.email, 
			subject: 'Email for Forget Password',
			text: 'Your Password',
			html: `Dear user your password is : ${password}`
        };
        // Sending mail
        transporter.sendMail(mailOptions, (err, data) => {
            if(err) return console.log(err);
            console.log(`Email Send to : ${user.email}`);
            res.status(200).json({ status: "success", message: `Passowrd send to : ${user.email}` });
            User.update({ password: password}, { where: { email: user.email }}).then( user => {});
        });
    } catch (error) {
        return res.status(401).json({ status: "failed", error });
    }
}

// Reset password
exports.resetPassword = async (req, res) => {
    // Validate request
    (!req.body.currentPassword) ? res.status(401).json({ status: "failed", message: "Current Password should not be empty!" }) : true;
    (!req.body.newPassword) ? res.status(401).json({ status: "failed", message: "New Password should not be empty!" }) : true;
    (!req.body.confirmPassword) ? res.status(401).json({ status: "failed", message: "Confirm Password should not be empty!" }) : true;
    (req.body.confirmPassword !== req.body.newPassword) ? res.status(401).json({ status: "failed", message: "New & Confirm Password not matching!" }) : true;
    // Get user by Id
    try {
        const user = await User.findOne({ where: { id: req.params.id } });
        if(!user) return res.status(401).json({ status: "failed", message: "User not found!" });
        if(user.password === req.body.currentPassword) {
            User.update({ password: req.body.confirmPassword }, { where: { id: req.params.id } }).then(user => {
                return res.status(200).json({success: 'success', message: "Password Changed!"});
            }).catch(err => {
                return res.status(401).json({ status: "failed", error });        
            })
        } else {
            return res.status(401).json({success: 'failed', message: "Current Password is Incorrect!"});
        }
    } catch (error) {
        return res.status(401).json({ status: "failed", error });
    }
}

function checkEmail(email) {
    const EmailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
	return EmailRegexp.test(email);
}