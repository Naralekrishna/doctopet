const express = require('express');
const bodyParser = require('body-parser');

const db = require('./app/configuration/dbConfig.js');

const app = express();
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
require('./app/routes/routes.js')(app);

app.use(express.static(`${__dirname}/public/images/`));

db.sequelize.sync({force: false, alter: true}).then(() => {
  console.log('Drop and Resync with { force: true }');
});
const port = 3000;
const server = app.listen(port, '0.0.0.0', () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`App listening on http://localhost:${port}`);
});